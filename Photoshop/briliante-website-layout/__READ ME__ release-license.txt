Brilliante Website Layout
(c) 2010 Spyre Studios


This website layout is provided, free of charge, for you to use in your projects - both personal and commercial. We encourage you to use them as you best see fit. However, your use of this layout design is subject to the following conditions, which you must agree before using them.


YOU MAY NOT:

- sell or otherwise profit from the distribution of this website layout

- distribute this website layout without proper credit and acknowledgement to the author (Spyre Studios or Mahmoud Khaled - one or the other or both).

- use this website layout for any use that would (or potentially could) be deemed pornographic, racist, hateful, criminal or otherwise damaging.


Thanks for understanding! And if you do use this website layout, I encourage you to drop us a line! We'd love to see in action! Send us an email here: www.spyrestudios.com/contact/


jon@spyrestudios.com
www.spyrestudios.com